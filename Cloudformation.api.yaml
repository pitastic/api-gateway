---

AWSTemplateFormatVersion: "2010-09-09"
Description: Api Gateway api.pitastic.ch

Parameters:
  Certificate:
    Description: Certificate ARN - from us-east-1
    Type: String
  DomainName:
    Description: domain name for dns records
    Type: String
    Default: pitastic.ch
  SubdomainName:
    Description: subdomain from which the api gateway is accessible
    Type: String
    Default: api

Resources:
  # create the custom domain name
  ApiCustomDomainName:
    Type: 'AWS::ApiGateway::DomainName'
    Properties:
      DomainName: !Sub "${SubdomainName}.${DomainName}"
      CertificateArn: !Ref Certificate

  # iam role to create logstreams for the apis
  ApiCloudWatchLogsRole:
    Type: "AWS::IAM::Role"
    Properties:
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          -
            Effect: "Allow"
            Principal:
              Service:
                - "apigateway.amazonaws.com"
            Action:
              - "sts:AssumeRole"

  ApiCloudWatchLogsPolicy:
    Type: "AWS::IAM::Policy"
    Properties: 
      PolicyName: "root"
      PolicyDocument: 
        Version: "2012-10-17"
        Statement: 
          - 
            Effect: "Allow"
            Action: 
              - "logs:CreateLogGroup"
              - "logs:CreateLogStream"
              - "logs:DescribeLogGroups"
              - "logs:DescribeLogStreams"
              - "logs:PutLogEvents"
              - "logs:GetLogEvents"
              - "logs:FilterLogEvents"
            Resource: "*"
      Roles: 
        - 
          Ref: "ApiCloudWatchLogsRole"

  # api gateway account with cloudwatch permissions
  ApiGatewayAccount:
    Type: "AWS::ApiGateway::Account"
    DependsOn: ApiCloudWatchLogsPolicy
    Properties:
      CloudWatchRoleArn: !GetAtt ApiCloudWatchLogsRole.Arn

  # add dns entry for api.pitastic.ch
  R53Entry:
    Type: "AWS::Route53::RecordSet"
    Properties: 
      AliasTarget: 
        # hosted zone for cloudfront
        # https://docs.aws.amazon.com/general/latest/gr/rande.html#apigateway_region
        HostedZoneId: Z2FDTNDATAQYW2
        DNSName: !GetAtt ApiCustomDomainName.DistributionDomainName
      Comment: Route to CloudFront distribution
      HostedZoneName: !Sub "${DomainName}."
      Name: !Sub "${SubdomainName}.${DomainName}."
      Type: A

Outputs:
  CustomDomainName:
    Value: !Sub "${SubdomainName}.${DomainName}"
    Export:
      Name: ApiCustomDomain